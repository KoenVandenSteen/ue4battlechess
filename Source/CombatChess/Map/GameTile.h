// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/SelectableObjectInterface.h"
#include "GameTile.generated.h"

class AMyPlayerController;
class GameMap;

UCLASS()
class COMBATCHESS_API AGameTile : public AActor, public ISelectableObjectInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameTile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	

	virtual void OnObjectSelected() override;

	virtual void OnObjectUnselected() override;

	virtual void OnMouseOver();

	virtual void OnMouseLeave();

	void Init(UStaticMesh* tileMesh, UMaterialInstance *tileMat, FVector2D tileCoord);

	void SetTileMaterial(FLinearColor color);
	void ResetTileMaterial();

	FVector2D GetTileCoord() 
	{
		return _tileCoord;
	}

private:

	FVector2D _tileCoord;
	UStaticMeshComponent *_myGameTilePtr = nullptr;
	AMyPlayerController *_myPlayerController = nullptr;
	UMaterialInstanceDynamic *_tileDynamicMaterial = nullptr;
	bool _isZoomedIn = false;
};
