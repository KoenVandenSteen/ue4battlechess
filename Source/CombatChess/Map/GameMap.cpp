// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "GameMap.h"
#include "GameTile.h"

GameMap::GameMap()
{

}

GameMap::~GameMap()
{
	for (auto tile : _gameMapVector)
	{
		delete tile;
		tile = nullptr;
	}
	_gameMapVector.clear();
}

void GameMap::AddNewGameTile(AGameTile * newTile)
{
	_gameMapVector.push_back(newTile);
}

AGameTile * GameMap::GetGameTile(int x, int y)
{
	int mapsize = (int)sqrt(_gameMapVector.size());


	int X = x;
	int Y = y + x / 2;

	//return the tile based on the axial coordinate

	if (X >= 0 && X < mapsize && Y >= 0 && Y < mapsize)
	{
		return _gameMapVector.at(X + Y*mapsize); //[r][ q + r / 2];
	}
	else
	{
		return nullptr;
	}
}

AGameTile * GameMap::GetGameTile(FVector2D coord)
{
	int mapsize = (int)sqrt(_gameMapVector.size());


	int X = coord.X;
	int Y = coord.Y + coord.X / 2;
		
	//return the tile based on the axial coordinate

	if (X >= 0 && X < mapsize && Y >= 0 && Y < mapsize)
	{
		return _gameMapVector.at(X + Y*mapsize); //[r][ q + r / 2];
	}
	else
	{
		return nullptr;
	}
}

std::vector<AGameTile*> GameMap::GetNeighBours(FVector2D tileCoord)
{
	std::vector<AGameTile *> neighbours;

	for (auto offset : _directions)
	{
		auto neighBour = GetGameTile(tileCoord + offset); 
		if (neighBour)
		{
			neighbours.push_back(neighBour);
		}
	}
	return neighbours;
}

