// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "GameTile.h"
#include "../Player/MyPlayerController.h"
#include "GameMap.h"
// Sets default values
AGameTile::AGameTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_myGameTilePtr = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MY TYLE"));
	RootComponent = _myGameTilePtr;
	OnClicked.AddDynamic(this, &AGameTile::OnObjectSelected);
	OnBeginCursorOver.AddDynamic(this, &AGameTile::OnMouseOver);
	OnEndCursorOver.AddDynamic(this, &AGameTile::OnMouseLeave);
}

// Called when the game starts or when spawned
void AGameTile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGameTile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AGameTile::Init(UStaticMesh* tileMesh, UMaterialInstance *tileMat,FVector2D tileCoord)
{
	//initalize static mesh4

	_myGameTilePtr->SetStaticMesh(tileMesh);
	_myGameTilePtr->SetWorldScale3D(FVector(5.f));
	_tileDynamicMaterial = UMaterialInstanceDynamic::Create(tileMat, this);
	_myGameTilePtr->SetMaterial(0, _tileDynamicMaterial);
	_myPlayerController = (AMyPlayerController*)GetWorld()->GetFirstPlayerController();
	_tileCoord = tileCoord;
}

void AGameTile::OnObjectSelected()
{
	if (_myPlayerController->GetSelectedTile() == this)
	{
		return;
	}

	if (!_isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() * 1.1f);
		_isZoomedIn = true;
	}

	_myPlayerController->SetSelectedTile(this);
}

void AGameTile::OnObjectUnselected()
{
	if (_isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() / 1.1f);
		_isZoomedIn = false;
	}
}

void AGameTile::OnMouseOver()
{
	if (_myPlayerController->GetSelectedPawn() && !_isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() * 1.1f);
		_isZoomedIn = true;
	}
}

void AGameTile::OnMouseLeave()
{
	if (_myPlayerController->GetSelectedPawn() && _isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() / 1.1f);
		_isZoomedIn = false;
	}
}


void AGameTile::SetTileMaterial(FLinearColor color)
{
	_tileDynamicMaterial->SetVectorParameterValue(FName(TEXT("PlayerColor")), color);
}

void AGameTile::ResetTileMaterial()
{
	_tileDynamicMaterial->SetVectorParameterValue(FName(TEXT("PlayerColor")), FLinearColor(1.0, 1.0, 1.0, 1.0));
}