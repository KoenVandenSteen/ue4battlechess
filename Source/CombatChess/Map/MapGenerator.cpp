// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "MapGenerator.h"

#include "GameTile.h"
#include "../CombatChessGameMode.h"
#include <vector>
#include "GameMap.h"

// Sets default values
AMapGenerator::AMapGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMapGenerator::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMapGenerator::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AMapGenerator::CreateGameMap(int mapsize, float hexSize)
{
	UWorld *const world = GetWorld();
	auto myGame = (ACombatChessGameMode*)world->GetAuthGameMode();
	float height = hexSize * 2.f;
	float vertOffset = height * 3.f / 4.f;
	float width = sqrt(3.f) / 2.f * height;
	float offset = width / 2.f;

	if (myGame->GetGameMap())
		delete myGame->GetGameMap();

	GameMap* newGameMap = new GameMap();

	for (int i = 0; i < mapsize*mapsize; i++)
	{
		int x = i % mapsize;
		int y = i / mapsize;

		if (i % 2 == 0)
			offset = width / 2.f;
		else
			offset = 0;

		FTransform newTransform = FTransform(FVector(-x*vertOffset, y*width - offset, 0));
		auto obj = world->SpawnActor<AGameTile>(AGameTile::StaticClass(), newTransform);

		//[r][ q + r / 2];

		int indexX = x;
		int indexY = y - (x / 2);

		obj->Init(UTileMesh, UTileMaterial, FVector2D(indexX, indexY));
		newGameMap->AddNewGameTile(obj);
	}

	myGame->SetGameMap(newGameMap);

}