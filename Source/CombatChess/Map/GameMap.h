// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <vector>

class AGameTile;


class COMBATCHESS_API GameMap
{
public:
	GameMap();
	~GameMap();

	void AddNewGameTile(AGameTile* newTile);

	AGameTile* GetGameTile(int x, int y);
	AGameTile* GetGameTile(FVector2D);

	std::vector<AGameTile*> GetNeighBours(FVector2D tileCoord);
	
private:
	//vector with all possible directions arround a certain point
	std::vector<FVector2D> _directions = {FVector2D(+1,0),FVector2D(+1,-1) ,FVector2D(0,-1) ,FVector2D(-1,0) ,FVector2D(-1,+1) ,FVector2D(0,+1)};

	std::vector<AGameTile*> _gameMapVector;
};
