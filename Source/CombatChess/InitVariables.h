// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "InitVariables.generated.h"

UCLASS()
class COMBATCHESS_API AInitVariables : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInitVariables();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	static AInitVariables* GetInitVars() { return _thisInitVariable; }

	float GetCameraMouseMoveMargin() const  { return CameraMouseMoveMargin; }
	float GetCameraMoveSpeed() const { return CameraMoveSpeed; }
	float GetCameraZoomSpeed()const { return CameraZoomSpeed; }
	float GetCameraMaxZoom() const { return CameraMaxZoom; }
	float GetCameraMinZoom() const { return CameraMinZoom; }
	FVector GetCameraStartPos()  const { return CameraStartPos; }
	bool IsScreenLocked() const { return LockScreen; }
	UStaticMesh *GetBasePawnMesh() const { return BasePawnMesh; }
	UMaterial *GetPawnBaseMaterial() const { return PawnBaseMat; }
	UMaterial *GetPlayer1Material() const { return Player1Mat; }
	UMaterial *GetPlayer2Material() const { return Player2Mat; }

protected:
	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraMouseMoveMargin;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraMoveSpeed;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraZoomSpeed;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraMaxZoom;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraMinZoom;

	UPROPERTY(EditAnywhere, Category = "Camera")
	FVector CameraStartPos;

	UPROPERTY(EditAnywhere, Category = "Camera")
	bool LockScreen = true;

	UPROPERTY(EditAnywhere, Category = "Pawn")
	UStaticMesh *BasePawnMesh;

	UPROPERTY(EditAnywhere, Category = "Pawn")
	UMaterial *PawnBaseMat;

	UPROPERTY(EditAnywhere, Category = "Pawn")
	UMaterial *Player1Mat;

	UPROPERTY(EditAnywhere, Category = "Pawn")
	UMaterial *Player2Mat;




private:
	static AInitVariables *_thisInitVariable;

};
