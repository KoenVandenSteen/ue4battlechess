// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "PlayerPawn.h"
#include "../InitVariables.h"
#include "../Player/MyPlayerController.h"
// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_playerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MY PAWN"));
	RootComponent = _playerMesh; 
	OnClicked.AddDynamic(this, &APlayerPawn::OnObjectSelected);
	OnBeginCursorOver.AddDynamic(this, &APlayerPawn::OnMouseOver);
	OnEndCursorOver.AddDynamic(this, &APlayerPawn::OnMouseLeave);
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

void  APlayerPawn::Init()
{
	auto variables = AInitVariables::GetInitVars();
	_playerMesh->SetStaticMesh(variables->GetBasePawnMesh());
	_playerMesh->SetWorldScale3D(FVector(1.f));
	_playerMesh->SetMaterial(0, variables->GetPawnBaseMaterial());
	_playerMesh->SetMaterial(1, variables->GetPlayer1Material());

	FVector ground;
	FVector size;
	GetActorBounds(false, ground, size);
	_sizeOffset = FVector(size.X / 2, size.Y / 2, 0);
	
	_myPlayerController = (AMyPlayerController*)GetWorld()->GetFirstPlayerController();
}


// Called every frame
void APlayerPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void APlayerPawn::OnObjectSelected()
{
	if (_myPlayerController->GetSelectedPawn() == this)
	{
		return;
	}
	_myPlayerController->SetSelectedPawn(this);

	if (!_isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() * 1.2f);
		_isZoomedIn = true;
	}
}

void APlayerPawn::OnObjectUnselected()
{
	if (_isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() / 1.2f);
		_isZoomedIn = false;
	}
}

void APlayerPawn::OnMouseOver()
{
	if (_myPlayerController->GetSelectedPawn() != this && !_isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() * 1.2f);
		_isZoomedIn = true;
	}
}

void APlayerPawn::OnMouseLeave()
{
	if (_myPlayerController->GetSelectedPawn() != this && _isZoomedIn)
	{
		SetActorScale3D(GetActorScale3D() / 1.2f);
		_isZoomedIn = false;
	}
}

