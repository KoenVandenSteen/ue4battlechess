// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/SelectableObjectInterface.h"
#include "PlayerPawn.generated.h"

class AMyPlayerController;

UCLASS()
class COMBATCHESS_API APlayerPawn : public AActor, public ISelectableObjectInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


	virtual void OnObjectSelected();
	virtual void OnObjectUnselected();
	virtual void OnMouseOver();
	virtual void OnMouseLeave();

	void Init();

	FVector GetSizeOffset() 
	{
		return _sizeOffset;
	}


private:
	UStaticMeshComponent *_playerMesh = nullptr;
	FVector _sizeOffset;
	AMyPlayerController *_myPlayerController = nullptr;
	bool _isZoomedIn = false;
};
