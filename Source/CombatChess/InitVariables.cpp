// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "InitVariables.h"

AInitVariables* AInitVariables::_thisInitVariable = nullptr;
// Sets default values
AInitVariables::AInitVariables()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_thisInitVariable = this;

}

// Called when the game starts or when spawned
void AInitVariables::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AInitVariables::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

