// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "CameraPawn.generated.h"

UCLASS()
class COMBATCHESS_API ACameraPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACameraPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void Init(float cameraspeed, float zoomspeed, float camerMaxZoom, float cameraMinZoom, float mouseMoveMargin, FVector spawnPos);
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;



protected:

	UPROPERTY(EditAnywhere)
		USpringArmComponent* CameraSpringArm;

	UCameraComponent* Camera;
	float _mouseMoveMargin;
	float _cameraMoveSpeed;
	float _cameraZoomSpeed;
	float _cameraMaxZoom;
	float _cameraMinZoom;
	FVector _spawnPos;

private:
	//Input functions
	//Input functions
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void ZoomIn();
	void ZoomOut();
	void MoveCameraWithMouse(float DeltaTime);

	//Input variables

	FVector2D _movementInput;
	float _zoomInput;
	FVector2D _previousMousePos;
	APlayerController *_thisPlayerController;



};
