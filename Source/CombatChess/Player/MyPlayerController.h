// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

class APlayerPawn;
class AGameTile;
class GameMap;
/**
 * 
 */

UCLASS(config = Game)
class COMBATCHESS_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMyPlayerController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupInputComponent() override;

	void UnSelectObject();

	void SetSelectedTile(AGameTile *selectedTile);
	AGameTile * GetSelectedTile();

	void SetSelectedPawn(APlayerPawn *selectedPawn);
	APlayerPawn * GetSelectedPawn();


private:

	void ClickTile();

	APlayerPawn *_selectedPawn = nullptr;
	AGameTile *_selectedTile = nullptr;
	GameMap *_curGameMap = nullptr;
};
