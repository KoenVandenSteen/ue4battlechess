// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "CameraPawn.h"
#include "Engine.h"
#include "../InitVariables.h"

// Sets default values
ACameraPawn::ACameraPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create our components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraSpringArm->AttachTo(RootComponent);
	CameraSpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.f), FRotator(-75.0f,0.0f, 0.0f));
	CameraSpringArm->TargetArmLength = 5000.f;
	CameraSpringArm->bEnableCameraLag = true;
	CameraSpringArm->CameraLagSpeed = 3.0f;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
	Camera->AttachTo(CameraSpringArm, USpringArmComponent::SocketName);

	//Take control of the default Player
	//AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void ACameraPawn::BeginPlay()
{
	Super::BeginPlay();	
	FString debugString = "begin game: " + FString::FromInt(_cameraMoveSpeed) + " " + FString::FromInt(_cameraZoomSpeed);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, debugString);
}

void ACameraPawn::Init(float cameraspeed, float zoomspeed, float camerMaxZoom, float cameraMinZoom, float mouseMoveMargin, FVector spawnPos)
{
	_mouseMoveMargin = mouseMoveMargin;
	_cameraMoveSpeed = cameraspeed;
	_cameraZoomSpeed = zoomspeed;
	_cameraMaxZoom = camerMaxZoom;
	_cameraMinZoom = cameraMinZoom;
	_spawnPos = spawnPos;
	Camera->FieldOfView = _cameraMaxZoom;
	SetActorLocation(spawnPos);
	SetActorRotation(FQuat::Identity);
}

// Called every frame
void ACameraPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//FString debugString = FString::FromInt(_cameraMoveSpeed) + " " + FString::FromInt(_cameraZoomSpeed);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, debugString);

	//check if player controller is initalized already, else do so
	if (!_thisPlayerController)
		_thisPlayerController = GetWorld()->GetFirstPlayerController();
	else
	{
		MoveCameraWithMouse(DeltaTime);
	}
	
	if (!_movementInput.IsZero())
	{
 		_movementInput = _movementInput.GetSafeNormal() * _cameraMoveSpeed;
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * _movementInput.X * DeltaTime;
		NewLocation += GetActorRightVector() * _movementInput.Y * DeltaTime;
		SetActorLocation(NewLocation);
	}

	if (_zoomInput != 0)
	{
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorUpVector() * _zoomInput * DeltaTime * _cameraZoomSpeed;
		SetActorLocation(NewLocation);
	}
}

void ACameraPawn::MoveCameraWithMouse(float DeltaTime)
{
	FVector2D viewportSize;
	if (GEngine)
	{
		viewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	}
	float x = -1;
	float y = -1;
	_thisPlayerController->GetMousePosition(x, y);

	//FString debugString = FString::FromInt(x) + " " + FString::FromInt(y);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, debugString);
	if (AInitVariables::GetInitVars()->IsScreenLocked())
	{
		FInputModeGameAndUI myInputmode;
		myInputmode.SetLockMouseToViewport(true);
		_thisPlayerController->SetInputMode(myInputmode);
	}

	FVector newLocation = GetActorLocation();

	if (_previousMousePos.X < _mouseMoveMargin )
	{
		newLocation -= _cameraMoveSpeed * DeltaTime * GetActorRightVector();
	}
	else if (_previousMousePos.X > viewportSize.X - _mouseMoveMargin)
	{
		newLocation += _cameraMoveSpeed * DeltaTime* GetActorRightVector();
	}

	if (_previousMousePos.Y < _mouseMoveMargin)
	{
		newLocation += _cameraMoveSpeed * DeltaTime * GetActorForwardVector();
	}
	else if (_previousMousePos.Y > viewportSize.Y - _mouseMoveMargin)
	{
		newLocation -= _cameraMoveSpeed * DeltaTime * GetActorForwardVector();
	}

	SetActorLocation(newLocation);

	if (x >= 0)
		_previousMousePos.X = x;
	if (y >= 0)
		_previousMousePos.Y = y;
}
// Called to bind functionality to input
void ACameraPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis("MoveForward", this, &ACameraPawn::MoveForward);
	InputComponent->BindAxis("MoveSideward", this, &ACameraPawn::MoveRight);
	InputComponent->BindAction("CameraZoomIn", IE_Pressed, this , &ACameraPawn::ZoomIn);
	InputComponent->BindAction("CameraZoomOut", IE_Pressed, this, &ACameraPawn::ZoomOut);
}

void ACameraPawn::ZoomIn()
{
	if (Camera->FieldOfView > _cameraMinZoom)
	{
		Camera->FieldOfView -= _cameraZoomSpeed;
	}
}

void ACameraPawn::ZoomOut()
{
	if (Camera->FieldOfView < _cameraMaxZoom)
	{
		Camera->FieldOfView += _cameraZoomSpeed;
	}
}
//Input functions
void ACameraPawn::MoveForward(float AxisValue)
{
	_movementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void ACameraPawn::MoveRight(float AxisValue)
{
	_movementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}


