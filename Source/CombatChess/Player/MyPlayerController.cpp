// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "MyPlayerController.h"
#include "../Map/GameTile.h"
#include "../PlayPieces/PlayerPawn.h"
#include "Map/GameMap.h"
#include "../CombatChessGameMode.h"
AMyPlayerController::AMyPlayerController() 
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}


void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("MouseLeft", EInputEvent::IE_Pressed, this, &AMyPlayerController::ClickTile);
	InputComponent->BindAction("MouseRight", EInputEvent::IE_Pressed, this, &AMyPlayerController::UnSelectObject);
}

void AMyPlayerController::UnSelectObject()
{
	if (_selectedTile)
	{
		_selectedTile->OnObjectUnselected();
		_selectedTile = nullptr;
	}

	if (_selectedPawn)
	{
		_selectedPawn->OnObjectUnselected();
		_selectedPawn = nullptr;		
	}

}

void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void AMyPlayerController::Tick(float deltaSeconds)
{	
	Super::Tick(deltaSeconds);

	if (!_curGameMap)
		_curGameMap = ((ACombatChessGameMode*)GetWorld()->GetAuthGameMode())->GetGameMap();

	_curGameMap->GetGameTile(FVector2D(0,0))->SetTileMaterial(FLinearColor(1, 0, 0, 1));
}

void AMyPlayerController::ClickTile()
{


	if (_selectedTile && !_selectedPawn)
	{
		FTransform newTransform = FTransform(_selectedTile->GetActorLocation());
		auto obj = GetWorld()->SpawnActor<APlayerPawn>(APlayerPawn::StaticClass(), newTransform);
		obj->Init();
		obj->SetActorLocation(obj->GetActorLocation() + obj->GetSizeOffset());
		_selectedTile->OnObjectUnselected();
		_selectedTile = nullptr;
		
	}
	else if (_selectedPawn && _selectedTile)
	{
		FTransform newTransform = FTransform(_selectedTile->GetActorLocation());
		_selectedPawn->SetActorLocation(_selectedTile->GetActorLocation() + _selectedPawn->GetSizeOffset());
		_selectedPawn->OnObjectUnselected();
		_selectedPawn = nullptr;	
		_selectedTile->OnObjectUnselected();
		_selectedTile = nullptr;
	}

}

void AMyPlayerController::SetSelectedTile(AGameTile *selectedTile)
{
	if (!_selectedTile)
	{
		_selectedTile = selectedTile;	
		auto tile = _curGameMap->GetGameTile(_selectedTile->GetTileCoord());
		tile->SetTileMaterial(FLinearColor(1, 0, 0, 1));

		auto neigbors = _curGameMap->GetNeighBours(_selectedTile->GetTileCoord());

		for (auto neighbor : neigbors)
		{
			if (neighbor)
			{
				neighbor->SetTileMaterial(FLinearColor(1, 0, 0, 1));
			}
		}

		return;
	}

	if (_selectedTile != selectedTile)
	{
		_selectedTile->OnObjectUnselected();
		_selectedTile = selectedTile;
	}
}

AGameTile * AMyPlayerController::GetSelectedTile()
{
	return _selectedTile;
}

void AMyPlayerController::SetSelectedPawn(APlayerPawn * selectedPawn)
{

	if (!_selectedPawn)
	{
		_selectedPawn = selectedPawn;
		return;
	}

	if (_selectedPawn != selectedPawn)
	{
		_selectedPawn->OnObjectUnselected();
		_selectedPawn = selectedPawn;
	}

}

APlayerPawn * AMyPlayerController::GetSelectedPawn()
{
	return _selectedPawn;
}
