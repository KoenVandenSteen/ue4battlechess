// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "CombatChessGameMode.generated.h"

class GameMap;
/**
 * 
 */
UCLASS()
class COMBATCHESS_API ACombatChessGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	virtual void StartPlay() override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	ACombatChessGameMode(const FObjectInitializer& ObjectInitializer);

	GameMap * GetGameMap();
	void SetGameMap(GameMap* newMap);



private:
	void SpawnController();

	GameMap * MyGameMapPtr;
	float m_RunningTime = 0;
	bool _cameraInitalized = false;
};
