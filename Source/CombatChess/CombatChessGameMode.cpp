// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatChess.h"
#include "CombatChessGameMode.h"

#include "Engine.h"
#include "Map/MapGenerator.h"
#include "Map/GameMap.h"
#include "Map/GameTile.h"
#include "Player/CameraPawn.h"
#include "Player/MyPlayerController.h"
#include "InitVariables.h"

ACombatChessGameMode::ACombatChessGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MyGameMapPtr = nullptr;
	PlayerControllerClass = AMyPlayerController::StaticClass();
}



void ACombatChessGameMode::StartPlay()
{
	SpawnController();
	Super::StartPlay();

	//Match Start();

	UWorld* thisWorld = GetWorld();

	for (TActorIterator<AMapGenerator> ActorItr(thisWorld); ActorItr; ++ActorItr)
	{
		ActorItr->CreateGameMap(40, 50);
	}
}

void ACombatChessGameMode::SpawnController()
{
	DefaultPawnClass = ACameraPawn::StaticClass();
}

void ACombatChessGameMode::BeginPlay()
{
	((ACameraPawn*)GetWorld()->GetFirstPlayerController()->GetPawn())->Init(
		AInitVariables::GetInitVars()->GetCameraMoveSpeed(),
		AInitVariables::GetInitVars()->GetCameraZoomSpeed(),
		AInitVariables::GetInitVars()->GetCameraMaxZoom(),
		AInitVariables::GetInitVars()->GetCameraMinZoom(),
		AInitVariables::GetInitVars()->GetCameraMouseMoveMargin(),
		AInitVariables::GetInitVars()->GetCameraStartPos()
		);
}

void ACombatChessGameMode::Tick(float DeltaSeconds)
{

	Super::Tick(DeltaSeconds);

}

GameMap * ACombatChessGameMode::GetGameMap()
{
	return MyGameMapPtr; 
}
void ACombatChessGameMode::SetGameMap(GameMap* newMap)
{
	MyGameMapPtr = newMap; 
}

