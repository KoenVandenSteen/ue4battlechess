// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SelectableObjectInterface.generated.h"


UINTERFACE()
class USelectableObjectInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class COMBATCHESS_API ISelectableObjectInterface
{
	GENERATED_IINTERFACE_BODY()

	UFUNCTION()
	virtual void OnObjectSelected() = 0;
	UFUNCTION()
	virtual void OnObjectUnselected() = 0;
	UFUNCTION()
	virtual void OnMouseOver() = 0;
	UFUNCTION()
	virtual void OnMouseLeave() = 0;
};